FROM maven:3.6.1-alpine

WORKDIR /usr/app

COPY ../CodacyTests ./CodacyTests

WORKDIR /usr/app/CodacyTests

COPY ../run-tests.sh ./run-tests.sh
