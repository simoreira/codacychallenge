package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class LoginPage {
    protected WebDriver driver;

    private By githubLogin = By.id("btn-login-github");

    public LoginPage(WebDriver driver){
        this.driver = driver;
      }

  public void clickLoginWithGithub() {
    Assert.assertEquals(true, driver.findElement(githubLogin).isDisplayed());
    driver.findElement(githubLogin).click();
  }
}