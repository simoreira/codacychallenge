package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RepositoriesPage {

    protected WebDriver driver;
    private By addRepositoriesAlert = By.id("add-repositories");
    private By addNewRepositoryPopUpButton = By.cssSelector("#add-repositories > div.css-5fesf6.e42ql693 > div > div > div > table > tbody > tr > td:nth-child(3) > a");
    private By repositoryName = By.cssSelector("#add-repositories > div.css-5fesf6.e42ql693 > div > div > div > table > tbody > tr > td:nth-child(1) > div > p");
    private By navigateToRepository = By.cssSelector("#add-repositories > div.css-5fesf6.e42ql693 > div > div > div > table > tbody > tr > td:nth-child(3) > a");

    public RepositoriesPage(WebDriver driver){
        this.driver = driver;
      }

    public void assertAddRepositoryAlertIsVisible() {
        waitUntilVisible(addRepositoriesAlert);
    }

    public void assertAndaddNewRepositoryFromPopUp() {
        waitUntilVisible(repositoryName);
        Assert.assertEquals("DietPi", driver.findElement(repositoryName).getText());
        Assert.assertEquals(true, driver.findElement(addNewRepositoryPopUpButton).isDisplayed());
        driver.findElement(addNewRepositoryPopUpButton).click();
    }

    public void navigateToAddedRepository() {
        waitUntilVisible(navigateToRepository);
        driver.findElement(navigateToRepository).click();
    }

    public void waitUntilVisible(By element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
    }

}