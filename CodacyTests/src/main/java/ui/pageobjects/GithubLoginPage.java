package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class GithubLoginPage {
    protected WebDriver driver;

    private By integrationLoginForm = By.id("login");
    private By loginField = By.id("login_field");
    private By passwordField = By.id("password");
    private By loginButton = By.cssSelector("#login > div.auth-form-body.mt-3 > form > div > input.btn.btn-primary.btn-block");
    private By invalidLogin = By.className("container-lg px-2");

    public GithubLoginPage(WebDriver driver){
        this.driver = driver;
      }


  public void assertLoginWithGithubPage() {
    Assert.assertEquals(true, driver.findElement(integrationLoginForm).isDisplayed());
  }

public void enterEmail() {
    Assert.assertEquals(true, driver.findElement(loginField).isDisplayed());
    driver.findElement(loginField).sendKeys("codacychallenge@gmail.com");
}

public void enterPassword() {
    Assert.assertEquals(true, driver.findElement(passwordField).isDisplayed());
    driver.findElement(passwordField).sendKeys("Codacy2019");
}

public void clickLogin() {
    Assert.assertEquals(true, driver.findElement(loginButton).isDisplayed());
    driver.findElement(loginButton).click();
}


public void enterWrongEmail() {
    Assert.assertEquals(true, driver.findElement(loginField).isDisplayed());
    driver.findElement(loginField).sendKeys("test");
}

public void enterWrongPassword() {
    Assert.assertEquals(true, driver.findElement(passwordField).isDisplayed());
    driver.findElement(passwordField).sendKeys("test");
}
public void assertInvalidLoginMessage() {
    Assert.assertEquals(true, driver.findElement(invalidLogin).isDisplayed());
}
}