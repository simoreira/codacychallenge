package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SettingsPage {
    protected WebDriver driver;

    private By removeRepositoryButton = By.id("unregister_project_btn");
    private By areYouSureModal = By.cssSelector("#GeneralView > div.modal.fade.modal-remove-project.in > div > div");
    private By removeButton = By.cssSelector("#GeneralView > div.modal.fade.modal-remove-project.in > div > div > div.modal-footer > button.btn.btn-danger.btnRemoveProject");

    public SettingsPage(WebDriver driver){
        this.driver = driver;
    }
    public void clickRemoveRepository() {
        Assert.assertEquals(true, driver.findElement(removeRepositoryButton).isDisplayed());
        driver.findElement(removeRepositoryButton).click();
    }

    public void assertAreYouSurePopUp() {
        waitUntilVisible(areYouSureModal);
    }

    public void clickConfirmRemoveButton() {
        waitUntilVisible(removeButton);
        driver.findElement(removeButton).click();
    }

    public void waitUntilVisible(By element) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
    }
    
}