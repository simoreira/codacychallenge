package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DashboardPage {
    protected WebDriver driver;
    private String repoName = "DietPi";

    private By waitingForRepoMessage = By.cssSelector("#root > div > main > div.css-19gy49k.erud2ym2 > div > h1");
    private By dashboardPanel = By.cssSelector("#root > div > main > div.css-19gy49k.erud2ym2 > div");
    private By repoNameDashboard = By.cssSelector("#root > div > main > div.css-19gy49k.erud2ym2 > div > header > div.css-10nf7hq.erud2ym2 > h1");
    private By qualityEvolutionPanel = By.id("header-quality-evolution");
    private By settingsButton = By.id("settings-navigate");

    public DashboardPage(WebDriver driver){
        this.driver = driver;
    }
    public void waitForRepositoryLoading() {
        waitUntilVisible(waitingForRepoMessage);
        Assert.assertEquals("We're preparing " + repoName + " for you.", driver.findElement(waitingForRepoMessage).getText());
    }

    public void waitForDashboardPanel() {
        waitUntilVisible(dashboardPanel);
    }

    public void assertRepoName() {
        waitUntilVisible(repoNameDashboard);
        Assert.assertEquals(repoName, driver.findElement(repoNameDashboard).getText());
    }

    public void assertQualityEvolutionPanel() {
        waitUntilVisible(qualityEvolutionPanel);
        Assert.assertEquals(true, driver.findElement(qualityEvolutionPanel).isDisplayed());
    }

    public void navigateToSettings() {
        Assert.assertEquals(true, driver.findElement(settingsButton).isDisplayed());
        driver.findElement(settingsButton).click();
    }

    public void waitUntilVisible(By element) {
        WebDriverWait wait = new WebDriverWait(driver, 300);
        wait.until(ExpectedConditions.presenceOfElementLocated(element));
    }
    
}