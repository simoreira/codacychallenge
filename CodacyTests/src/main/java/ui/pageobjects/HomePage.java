package ui.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage {
    protected WebDriver driver;
    private By loginButton = By.id("qa-login-link");
    
    public HomePage(WebDriver driver){
      this.driver = driver;
    }
    
    public void navigateToLogin() {
      Assert.assertEquals(true, driver.findElement(loginButton).isDisplayed());
      driver.findElement(loginButton).click();
    }
}