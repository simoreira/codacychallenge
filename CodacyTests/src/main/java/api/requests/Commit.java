package api.requests;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import org.apache.http.HttpStatus;

public class Commit {
    String username;
    String projectName;
    String commitUUID;
    String url;

    public Commit(String username, String projectName, String commitUUID) {
        this.username = username;
        this.projectName = projectName;
        this.commitUUID = commitUUID;
        this.url = "https://api.codacy.com/2.0/" + username + "/" + projectName + "/commit/" + commitUUID + "";
    }

    public void getCommitStatus() {
        given()
        .headers("api_token", "yQi349TI4MCJ9MVixGUb", 
                "Accept", ContentType.JSON)
        .when()
        .get(url)
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_OK)
        .body("state", equalTo("Analysed"));
    }

}