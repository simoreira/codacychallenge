package codacy.challenge;

import ui.pageobjects.DashboardPage;
import ui.pageobjects.GithubLoginPage;
import ui.pageobjects.HomePage;
import ui.pageobjects.LoginPage;
import ui.pageobjects.RepositoriesPage;
import ui.pageobjects.SettingsPage;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class UiE2eTest {
    WebDriver driver;
    HomePage homePage;
    LoginPage loginPage;
    GithubLoginPage githubLoginPage;
    RepositoriesPage repositoriesPage;
    DashboardPage dashboardPage;
    SettingsPage settingsPage;

    @Before
    public void setup() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(BrowserType.CHROME);
        driver = new RemoteWebDriver(new URL("http://selenium-hub:4444/wd/hub"), capabilities);
        driver.get("http://www.codacy.com");
    }

    // I'd rather have a separeted setup with the different test cases, however, I can't get a before all behavior on JUnit. I tried BeforeClass but it requires a static method, which my "driver" can't be
    @Test
    public void MainTest() throws InterruptedException
    {
        homePage = new HomePage(driver);
        homePage.navigateToLogin();

        loginPage = new LoginPage(driver);
        loginPage.clickLoginWithGithub();

        // Disabling wrong login test because it fails most of the times due to the github policy of blocking IPs that fail to login multiple times

        // githubLoginPage = new GithubLoginPage(driver);
        // githubLoginPage.assertLoginWithGithubPage();
        // githubLoginPage.enterWrongEmail();
        // githubLoginPage.enterWrongPassword();
        // githubLoginPage.clickLogin();
        // githubLoginPage.assertInvalidLoginMessage();

        githubLoginPage = new GithubLoginPage(driver);
        githubLoginPage.assertLoginWithGithubPage();
        githubLoginPage.enterEmail();
        githubLoginPage.enterPassword();
        githubLoginPage.clickLogin();

        driver.get("https://app.codacy.com/organizations/gh/codacychallenge/repositories/add");

        repositoriesPage = new RepositoriesPage(driver);
        repositoriesPage.assertAddRepositoryAlertIsVisible();
        repositoriesPage.assertAndaddNewRepositoryFromPopUp();
        repositoriesPage.navigateToAddedRepository();

        dashboardPage = new DashboardPage(driver);

        dashboardPage.waitForRepositoryLoading();
        dashboardPage.waitForDashboardPanel();
        dashboardPage.assertRepoName();
        dashboardPage.assertQualityEvolutionPanel();
        dashboardPage.navigateToSettings();

        settingsPage = new SettingsPage(driver);

        settingsPage.clickRemoveRepository();
        settingsPage.assertAreYouSurePopUp();
        settingsPage.clickConfirmRemoveButton();

        repositoriesPage.assertAddRepositoryAlertIsVisible();
    }

    @After
    public void endSession() {
        driver.quit();
    }
    
    
}
