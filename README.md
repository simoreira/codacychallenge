# Codacy Challenge


### Getting Started
The main objective of this challenge is to have a test strategy, plan and automated tests (UI and API) for codacy.com.
The tests were developed with Selenium (UI) and Rest Assured (API). 
Docker and Maven are also used, the former to setup the environment and the latter to manage dependencies and builds.

#### Pre Requisites
- Docker Compose

### How to run the tests
1. Clone the repo

        $ git clone git@gitlab.com:simoreira/codacychallenge.git

2. The tests can be run by executing:

        $ cd Docker && docker-compose up --abort-on-container-exit

By default, the tests run against a Chrome node running on a Selenium Grid. If it is necessary to change the Browser, the tests can also be run against Firefox by changing the `capabilities.setBrowserName();` on the `@Before` tag on the UI test.

The tests run sequentially, first the UI and then the API.

### Tests

For the UI test, I thought about a E2E workflow where I would Login -> Add a new repository -> Assert some elements on the page -> Go to settings -> Delete repository. 

I followed the page object model and the classes can be found on `codacychallenge/CodacyTests/src/main/java/ui/pageobjects`.

For the API test, I retrieved the information on the required endpoint ensuring that a specific commit was analysed. 

## Test Strategy

The following section aims to describe the test strategy that ensures the quality of our product. The goal is not for this strategy to be final, but to evolve according to the experiences and defects found. 

Quality should be present in all the phases of the product lifecycle. Hence, all of the development phases have a corresponding test activity. For Codacy, this means that testing should begin as soon as a feature is planned but it never ends since feedback from the clients is also a good opportunity for quality improvements. 
With this in mind, the QA and development team should aim to prevent defects by keep a regression averse strategy, this is, ensuring that new features do not break already existent developments. However, there should also be a reactive strategy so that whenever a defect is found, reports and new tests are added to ensure that it will not happen again. Given the overall combined strategy, there is a strong focus on automation but manual testing is also fundamental. 

Codacy is a web application, therefore, the logic division for testing is the frontend and backend and their integration. However, there is also communication with third party services that should not be forgotten. Due to this, functional and non functional tests should be done. All the tests should be automated and run on regression with the exception of the non-functional tests. For each part, there should be the following test levels:

### Frontend
- **Component testing** 
    - Unit testing 
    - Visual testing
- **Component Integration testing**
    - Functional testing
    - Visual testing
- **Non Functional testing**
    - Performance testing

### Backend
- **Component testing**
    - Unit testing
- **Component integration testing**
    - Functional testing
- **Non Functional testing**
    - Performance testing

### Codacy (Backend + Frontend + External Services)
- **System integration testing**
- **System testing**
    - E2E testing
- **User Acceptance testing**
- **Non Functional testing**
    - Performance testing
    - Load testing

The UI tests should be done with Selenium and the backend with RestAssured. Docker should be used to provide a stable environment for the tests to run and ensure their portability. 
The non-functional tests (Performance and Load) should be done with Jmeter.

The tests that will comprise each test level described, should be planned since the first moment of the development lifecycle, so that the tests done at each level are correctly divided and there is no overlapping of testing. All of the described test types are candidates for automation and manual testing. 
Following the development lifecycle, from a high level perspective there is:

* Planning and Refinement;
* Development;
* Review;
* Internal deploy;
* Before Release.

Each of these phases contains different types of tests with different outcomes. 

#### Planning and Refinement

At plannings or refinements it is where the QAs have the first interaction with the features. The stakeholders (product owners, delivery/project managers and/or other stakeholders) present the features and what they expect from them. At this point, QAs and developers should review the expected behaviors with the QAs identifying the main scenarios, edge cases and overall inconsistencies or defects. The latter should be discussed with the stakeholders and there should be a clear understanding of which are the acceptance criteria. 

The expected outcome after these meetings should be a test plan that covers the main scenarios and edge cases identified. The QA should understand how the feature can be tested according each test level and develop a test plan for the corresponding phase. The test plan should be focused on the automated scenarios so that the corresponding tests can be developed later.

#### Development

During this phase, the tests defined on the test plans should be developed. This task should be either performed by the QA or the Developer. The end result should be test scripts that are assigned to the corresponding suites that run on the continuous integration framework.

#### Review

At review, the tasks depend on how the team works. If it is the QA developing the tests, then the developer should review them and vice versa. Nevertheless, the QA should always review the work done and test it. This is a manual step and the expected outcome is to ensure that the new developments work as expected. If some defects are found, then they should be informally communicated to the developer so they can be fixed. Codacy should also be used :) 
The performance and automated existent tests should be run before merging.

#### Internal deploy

After merging the work, all the existent functional test batteries should be run. This way, we ensure that every new version is a release candidate as long as it passes all the tests. A manual sanity check on the new features should be done by the QA and UAT should be done by the product team.
The test execution reports should be analyzed after all the tests run.

#### Before release
Given the previous workflow, the release candidate should be ready to be released. However, some last verifications are necessary. The QAs should perform core manual tests to ensure that the main features are working as expected, run the non functional tests and verify with the product team if there is any concern. 

After all validations are done, the package is ready to go to production.

### Test Plan

#### Test cases

Tests for automation to be run on regression:

 - When on commit page with maximum commits
    - should have branch switcher
    - should have commits list
        -  should have status column
        - should have author column
        - should have commit column
        - should have message column
        - should have created column
        - should have issues column
    - should have project switcher
    - should have options toolbar
    - should have previous and next buttons
- When switching between branches
    - should appear the current branch
    - should appear the button to select another branch
- When on commit page with at least one commit
    - should have information in all columns
    - the commit should be clickable
- When clicking on a commit
    - should navigate to the commit page
- When a commit introduces new issues
    - should have new issues on the issues column
- When a commit introduces new fixes
    - should have new fixes on the fixes column

Visual test:
- Maximum characters on each text component
- Selected tabs on the vertical tab bar
- Status field
- Merge tag
- Variations of number of commits and visibility of previous/next buttons
- Colors of the issues
- Color of the rows in the commits


